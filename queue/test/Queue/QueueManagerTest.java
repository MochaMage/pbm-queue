/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Queue;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mocha
 */
public class QueueManagerTest {
    /**
     * Test of addTutor method, of class QueueManager.
     */
    @Test
    public void testAddTutor() {
        System.out.println("\n\nTesting: addTutor\n\n");
        ArrayList<String> courses = new ArrayList<String>();
        courses.add("Discrete Math");
        courses.add("Calculus");
        courses.add("Algebra");
        Tutor t = new Tutor("Daniel Florez", courses);
        QueueManager instance = QueueManager.singleton();
        instance.clearAll();
        instance.addTutor(t);
        if(instance.addTutor(t))
            fail("Duplicate tutor added. Failure.");
    }

    /**
     * Test of currentTutors method, of class QueueManager.
     */
    @Test
    public void testCurrentStudents() 
    {
        System.out.println("\n\nTesting: currentTutors\n\n");
        QueueManager instance = QueueManager.singleton();        
        instance.clearAll();
        ArrayList expResult = new ArrayList();
        ArrayList result = instance.currentTutors();
        assertEquals(expResult, result);
        
    }

    /*
     * Tests the queueState method. 
     */
    @Test
    public void testQueueState() 
    {
        System.out.println("\n\nTesting: queueState\n\n");
        QueueManager instance = QueueManager.singleton();
        instance.clearAll();
        Request r = new Request("Danny", "Calculus");
        ArrayList expResult = new ArrayList();
        expResult.add(r);
        instance.addStudent(r);
        ArrayList result = instance.queueState();
        assertEquals(expResult, result);
    }

    /**
     * Tests the sitNextStudents method when the queue is empty.
     */
    @Test
    public void testSitNextStudentsEmptyQueue() 
    {
        QueueManager instance = QueueManager.singleton();
        instance.clearAll();
        String[] courses = {"Calculus","Algebra","Java","Discrete","C++"};
        ArrayList<String> wCourses = new ArrayList<String>();
        ArrayList<String> xCourses = new ArrayList<String>();
        ArrayList<String> yCourses = new ArrayList<String>();
        ArrayList<String> zCourses = new ArrayList<String>();
        wCourses.add(courses[0]);
        wCourses.add(courses[1]);
        wCourses.add(courses[2]);
        xCourses.add(courses[1]);
        yCourses.add(courses[0]);
        yCourses.add(courses[2]);
        yCourses.add(courses[3]);
        zCourses.add(courses[4]);
        Tutor w = new Tutor("Tutor one", wCourses);
        Tutor x = new Tutor("Tutor two", xCourses);
        Tutor y = new Tutor("Tutor three", yCourses);
        Tutor z = new Tutor("Tutor four", zCourses);
        instance.addTutor(w);
        instance.addTutor(x);
        instance.addTutor(y);
        instance.addTutor(z);
        instance.sitNextStudents();
        if(!instance.queueState().isEmpty())
            fail("Queue was not empty as expected.");
    }
    
    /**
     * Tests the sitNextStudents method when the queue size is smaller
     * than the tutor set size.
     */
    @Test
    public void testSitNextStudentsPartialQueue() 
    {
        for(int i = 0; i <= 50; i++)
            System.out.print("-");    
        System.out.println();
        System.out.println("PBM algorithm with Queue Size < Tutor set size");
        for(int i = 0; i <= 50; i++)
            System.out.print("-");    
        System.out.println();
        QueueManager instance = QueueManager.singleton();
        instance.clearAll();
        String[] courses = {"Calculus","Algebra","Java","Discrete","C++"};
        ArrayList<String> wCourses = new ArrayList<String>();
        ArrayList<String> xCourses = new ArrayList<String>();
        ArrayList<String> yCourses = new ArrayList<String>();
        ArrayList<String> zCourses = new ArrayList<String>();
        Request a = new Request("Student one",courses[0]);
        Request b = new Request("Student two",courses[1]);
        Request c = new Request("Student three",courses[2]);
        wCourses.add(courses[0]);
        wCourses.add(courses[1]);
        wCourses.add(courses[2]);
        xCourses.add(courses[1]);
        yCourses.add(courses[0]);
        yCourses.add(courses[2]);
        yCourses.add(courses[3]);
        zCourses.add(courses[4]);
        Tutor w = new Tutor("Tutor one", wCourses);
        Tutor x = new Tutor("Tutor two", xCourses);
        Tutor y = new Tutor("Tutor three", yCourses);
        Tutor z = new Tutor("Tutor four", zCourses);
        instance.addTutor(w);
        instance.addTutor(x);
        instance.addTutor(y);
        instance.addTutor(z);
        instance.addStudent(a);
        instance.addStudent(b);
        instance.addStudent(c);
        instance.sitNextStudents();
        ArrayList<Tutor> state = instance.currentTutors();
        System.out.format("CURRENT STATE OF QUEUE\n\n");
        System.out.format("%-15s%-15s\n", "Student", "Course");
        for(Request r : instance.queueState())
        {
            System.out.format("%-15s%-15s\n", r.getName(), r.getSubject());
        }
        System.out.format("\nSERVICE STATE");
        
        System.out.format("\n\n%-15s%-15s%-15s\n\n", "Tutor name", "Student", "Course");
        for(Tutor t: state)
        {
            if(t.getCurrent() != null)
            {
                System.out.format("%-15s%-15s%-15s\n", t.getName(), t.getCurrent().getName(), t.getCurrent().getSubject());
                if(!t.canTutor(t.getCurrent().getSubject()))
                    fail("A Tutor has been mismatched. Incompatible with student requirements");
            }
        }
        System.out.println();
    }
    
    /**
     * Tests the sitNextStudents method when the queue size is exactly
     * equal to the number of tutors available. 
     */
    @Test
    public void testSitNextStudentsFullQueue() 
    {
        for(int i = 0; i <= 50; i++)
            System.out.print("-");    
        System.out.println();
        System.out.println("PBM algorithm with Queue Size >= Tutor set size");
        for(int i = 0; i <= 50; i++)
            System.out.print("-");    
        System.out.println();
        QueueManager instance = QueueManager.singleton();
        instance.clearAll();
        String[] courses = {"Calculus","Algebra","Java","Discrete","C++"};
        ArrayList<String> wCourses = new ArrayList<String>();
        ArrayList<String> xCourses = new ArrayList<String>();
        ArrayList<String> yCourses = new ArrayList<String>();
        ArrayList<String> zCourses = new ArrayList<String>();
        Request a = new Request("Student one",courses[0]);
        Request b = new Request("Student two",courses[1]);
        Request c = new Request("Student three",courses[2]);
        Request d = new Request("Student four",courses[3]);
        Request e = new Request("Student five",courses[4]);
        Request f = new Request("Student six",courses[4]);
        wCourses.add(courses[0]);
        wCourses.add(courses[1]);
        wCourses.add(courses[2]);
        xCourses.add(courses[1]);
        yCourses.add(courses[0]);
        yCourses.add(courses[2]);
        yCourses.add(courses[3]);
        zCourses.add(courses[4]);
        Tutor w = new Tutor("Tutor one", wCourses);
        Tutor x = new Tutor("Tutor two", xCourses);
        Tutor y = new Tutor("Tutor three", yCourses);
        Tutor z = new Tutor("Tutor four", zCourses);
        instance.addTutor(w);
        instance.addTutor(x);
        instance.addTutor(y);
        instance.addTutor(z);
        instance.addStudent(a);
        instance.addStudent(b);
        instance.addStudent(c);
        instance.addStudent(d);
        instance.addStudent(e);   
        instance.addStudent(f);
        instance.sitNextStudents();
        ArrayList<Tutor> state = instance.currentTutors();
        System.out.format("CURRENT STATE OF QUEUE\n\n");
        System.out.format("%-15s%-15s\n", "Student", "Course");
        for(Request r : instance.queueState())
        {
            System.out.format("%-15s%-15s\n", r.getName(), r.getSubject());
        }
        System.out.format("\nSERVICE STATE");
        
        System.out.format("\n\n%-15s%-15s%-15s\n\n", "Tutor name", "Student", "Course");
        for(Tutor t: state)
        {
            if(t.getCurrent() != null)
            {
                System.out.format("%-15s%-15s%-15s\n", t.getName(), t.getCurrent().getName(), t.getCurrent().getSubject());
                if(!t.canTutor(t.getCurrent().getSubject()))
                    fail("A Tutor has been mismatched. Incompatible with student requirements");
            }            
        }
    }
    
    /**
     * Tests the sitNextStudents method over a course of 2 runs in
     * order to see the behavior of PBM over the the course of multiple runs.
     */
    @Test
    public void testSitNextStudentsMultipleRuns() 
    {
        for(int i = 0; i <= 50; i++)
            System.out.print("-");    
        System.out.println();
        System.out.println("PBM algorithm multiple runs.");
        for(int i = 0; i <= 50; i++)
            System.out.print("-");    
        System.out.println();
        QueueManager instance = QueueManager.singleton();
        instance.clearAll();
        String[] courses = {"Calculus","Algebra","Java","Discrete","C++"};
        ArrayList<String> wCourses = new ArrayList<String>();
        ArrayList<String> xCourses = new ArrayList<String>();
        ArrayList<String> yCourses = new ArrayList<String>();
        ArrayList<String> zCourses = new ArrayList<String>();
        Request a = new Request("Student one",courses[0]);
        Request b = new Request("Student two",courses[1]);
        Request c = new Request("Student three",courses[2]);
        Request d = new Request("Student four",courses[3]);
        Request e = new Request("Student five",courses[4]);
        Request f = new Request("Student six",courses[4]);
        wCourses.add(courses[0]);
        wCourses.add(courses[1]);
        wCourses.add(courses[2]);
        xCourses.add(courses[1]);
        yCourses.add(courses[0]);
        yCourses.add(courses[2]);
        yCourses.add(courses[3]);
        zCourses.add(courses[4]);
        Tutor w = new Tutor("Tutor one", wCourses);
        Tutor x = new Tutor("Tutor two", xCourses);
        Tutor y = new Tutor("Tutor three", yCourses);
        Tutor z = new Tutor("Tutor four", zCourses);
        instance.addTutor(w);
        instance.addTutor(x);
        instance.addTutor(y);
        instance.addTutor(z);
        instance.addStudent(a);
        instance.addStudent(b);
        instance.addStudent(c);
        instance.addStudent(d);
        instance.addStudent(e);   
        instance.addStudent(f);
        instance.sitNextStudents();
        ArrayList<Tutor> state = instance.currentTutors();
        System.out.format("CURRENT STATE OF QUEUE\n");
        System.out.format("\n%-15s%-15s\n\n", "Student", "Course");
        for(Request r : instance.queueState())
        {
            System.out.format("%-15s%-15s\n", r.getName(), r.getSubject());
        }
        System.out.format("\nSERVICE STATE\n");
        System.out.format("\n%-15s%-15s%-15s\n\n", "Tutor name", "Student", "Course");
        for(Tutor t: state)
        {
            if(t.getCurrent() != null)
            {
                System.out.format("%-15s%-15s%-15s\n", t.getName(), t.getCurrent().getName(), t.getCurrent().getSubject());
                if(!t.canTutor(t.getCurrent().getSubject()))
                    fail("A Tutor has been mismatched. Incompatible with student requirements");
            }        
        }
        instance.sitNextStudents();
        state = instance.currentTutors();
        System.out.format("\nCURRENT STATE OF QUEUE\n\n");
        System.out.format("%-15s%-15s\n", "Student", "Course");
        for(Request r : instance.queueState())
        {
            System.out.format("%-15s%-15s\n", r.getName(), r.getSubject());
        }
        System.out.format("\nSERVICE STATE\n");
        System.out.format("\n%-15s%-15s%-15s\n\n", "Tutor name", "Student", "Course");
        for(Tutor t: state)
        {
            if(t.getCurrent() != null)
            {
                System.out.format("%-15s%-15s%-15s\n", t.getName(), t.getCurrent().getName(), t.getCurrent().getSubject());
                if(!t.canTutor(t.getCurrent().getSubject()))
                    fail("A Tutor has been mismatched. Incompatible with student requirements");
            }        
        }
    }
}