/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Queue;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mocha
 */
public class RequestTest {
    /**
     * Test of getSubject method, of class Request.
     */
    @Test
    public void testGetSubject() {
        System.out.println("getSubject");
        String course = "Calculus";
        Request instance = new Request("Danny", course);
        String expResult = course;
        String result = instance.getSubject();
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class Request.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        String name = "Danny";
        Request instance = new Request(name, "Calculus");
        String expResult = name;
        String result = instance.getName();
        assertEquals(expResult, result);
    }
}