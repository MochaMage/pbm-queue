<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Queue.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%QueueManager qm = QueueManager.singleton();%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add a new tutor</title>
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />
    </head>
    <body>
        <div class="container-fluid" style="padding-left: 10%; padding-right: 10%; width: 80%">
            <div class=" row-fluid"style="background-color: #336699">
                <br><h1 class="text-center" style="color: #FFFFFF">Queue Monitor</h1><br>
            </div>
            <div class="navbar">
                <div class="navbar-inner">
                    <a href="index.jsp" class="brand">Monitor</a>
                    <ul class="nav">
                        <li><a href="newStudent.jsp">Add new student</a></li>
                        <li><a href="newTutor.jsp">Add new tutor</a></li>
                        <li><a href="tutorDetails.jsp">View Tutor Details</a></li>
                        <li><a href="clearAll.jsp" class='alert-danger'>Clear Everything</a></li>
                    </ul>
                </div>
            </div>
            <div class="hero-unit">
                <%
                    String name = request.getParameter("name");
                    String[] subjects = request.getParameterValues("subjects");
                    if(subjects == null || name.equals(""))
                    {
                        out.print("<h3 class='text-error text-center'>");
                        out.print("Invalid input. Please input both your name and at least one subject.");
                        out.print("<br/>");
                        out.print("<a href='newTutor.jsp' class='btn btn-primary'>Go back.</a>");
                        out.print("</h3>");
                    }
                    else
                    {
                        
                        Tutor t = new Tutor(name, new ArrayList<String>(Arrays.asList(subjects)));
                        if(qm.addTutor(t))
                        {
                            out.print("<h3 class='text-success'>");
                            out.print("New tutor <em>" + name + "</em> created.");
                            out.print("</h3><br/>");
                            out.print("<p class='text-info'>Chosen Subjects:</p>");
                            out.print("<table class='table table-striped table-bordered'>");
                            for(String s: subjects)
                                out.print("<tr><td><small class='pull-left'>" + s + "</small></td></tr>");
                            out.print("</table>");
                            out.print("<a href='index.jsp' class='btn btn-primary'>Go to monitor.</a>");
                        }
                        else
                        {
                            out.print("<h3 class='text-error'>");
                            out.print("Tutor name already exists. Please choose a different name.");
                            out.print("<br/>");
                            out.print("<a href='newTutor.jsp' class='btn btn-primary'>Go back.</a>");
                            out.print("</h3>");
                        }
                    }
                %>
            </div>
        </div>
    </body>
</html>