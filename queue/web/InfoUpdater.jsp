<%@page import="java.util.Calendar"%>
<%@page import="Queue.*"%>
<%@page import="org.json.JSONObject"%>
<%
    JSONObject json = new JSONObject();
    QueueManager qm = QueueManager.singleton();
    for(Request r: qm.queueState())
    {
        JSONObject j = new JSONObject();
        j.put("studentName", r.getName());
        j.put("subject", r.getSubject());
        json.append("queue", j);
    }
    for(Tutor t: qm.currentTutors())
    {
        JSONObject j = new JSONObject();
        j.put("tutor", t.getName());
        if(t.getCurrent() != null)
        {
            j.put("currentName", t.getCurrent().getName());
            j.put("currentSubject", t.getCurrent().getSubject());
            j.put("totalRequests", t.requestsServed());

        }
        else
        {
            j.put("currentName", "No student");
            j.put("currentSubject", "");            
            j.put("totalRequests", t.requestsServed());
        }
        json.append("current", j);
    }
    out.print(json);
    out.flush();
    System.out.println("InfoUpdater: Sending JSON to client: " + json);
%>
