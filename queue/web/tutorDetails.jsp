<%@page import="java.util.ArrayList"%>
<%@page import="Queue.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%QueueManager qm = QueueManager.singleton();%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Queue Monitor</title>
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />
    </head>
    <body>
        <div class="container-fluid" style="padding-left: 10%; padding-right: 10%; width: 80%">
            <div class=" row-fluid"style="background-color: #336699">
                <br><h1 class="text-center" style="color: #FFFFFF">Queue Monitor</h1><br>
            </div>
            <div class="navbar">
                <div class="navbar-inner">
                    <a href="index.jsp" class="brand">Monitor</a>
                    <ul class="nav">
                        <li><a href="newStudent.jsp">Add new student</a></li>
                        <li><a href="newTutor.jsp">Add new tutor</a></li>
                        <li><a href="tutorDetails.jsp">View Tutor Details</a></li>
                        <li><a href="clearAll.jsp" class='alert-danger'>Clear Everything</a></li>
                    </ul>
                </div>
            </div>
            <div class="hero-unit"> 
                <div class="row-fluid">
                    <h3>Current Tutors</h3>
                        <%
                        if(qm.currentTutors().isEmpty())
                            out.print("<h3 class='text-info text-center'>No tutors currently available</h3>");
                        else
                        {    
                            out.print("<table class='table table-bordered table-striped'>");
                            out.print("<tr><th>Tutor Name</th><th>Subjects</th></tr>");
                            for(Tutor t:qm.currentTutors())
                            {
                                out.print("<tr><td><b><small>" + t.getName() + "</small></b></td>");
                                out.print("<td><small class='text-info'>");
                                for(String s:t.getSubjects())
                                {
                                    out.print(s + "<br />");
                                }
                                out.print("</small></td>");
                                out.print("</tr>");
                            }
                            out.print("</table>");
                        }      
                        %>   
                </div>
            </div>
        </div>
    </body>
</html>