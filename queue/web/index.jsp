<%-- 
    Document   : QueueMonitor
    Created on : Jul 18, 2013, 10:02:24 AM
    Author     : Mocha
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="Queue.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%QueueManager qm = QueueManager.singleton();%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Queue Monitor</title>
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />
        <script>
            var ajax;
            var updater = setInterval(function(){show();}, 5000);
            window.onload = show();
            
            function xmlInit()
            {
                if(window.XMLHttpRequest)
                    {
                        ajax = new XMLHttpRequest();
                    }
                    else
                        {
                            ajax = new ActiveXObject("Microsoft.XMLHTTP");
                        }
            }
            
            function handler()
            {
                if(ajax.readyState == 4)
                    {
                        var json = JSON.parse(ajax.responseText);
                        var qTab = "<table class='table table-bordered table-striped'>";
                        var tTab = "<table class='table table-bordered table-striped'>";
                        qTab += "<tr><td><small>Student</small></td><td><small>Subject</small></td></tr>";
                        tTab += "<tr><td><small>Tutor name</small></td><td><small>Current student</small></td>" 
                                + "<td><small>Current subject</small></td><td><small>Total requests</small></td></tr>";
                        for(i in json.queue)
                            {
                                qTab += "<tr><td><small class='pull-left'>" 
                                        + json.queue[i].studentName 
                                        + "</td><td><small class='text-info pull-right'>"
                                        + json.queue[i].subject
                                        + "</small></td></small></td></tr>";
                            }
                        qTab += "</table>";  
                                
                        for(i in json.current)
                            {
                                tTab += "<tr><td><small class='pull-left'><b>"
                                        + json.current[i].tutor + "</b></td><td>" 
                                        + "<small class='text-info'>" 
                                        + json.current[i].currentName + "</td><td><small class='text-info'>"
                                        + json.current[i].currentSubject + "</small></td><td><small class='text-info'>"
                                        + json.current[i].totalRequests + "</small></td>"
                                        + "</small></small></td></tr>";
                            }
                        tTab += "</table>";
                        document.getElementById("Queue").innerHTML = qTab; 
                        document.getElementById("Tutors").innerHTML = tTab;
                    }
            }
    
    
            function show()
            {
                xmlInit();
                ajax.onreadystatechange = handler;
                ajax.open("GET", "InfoUpdater.jsp", true);
                ajax.send("");
            }
        </script>
    </head>
    <body>
        <div class="container-fluid" style="padding-left: 10%; padding-right: 10%; width: 80%">
            <div class=" row-fluid"style="background-color: #336699">
                <br><h1 class="text-center" style="color: #FFFFFF">Queue Monitor</h1><br>
            </div>
            <div class="navbar">
                <div class="navbar-inner">
                    <a href="index.jsp" class="brand">Monitor</a>
                    <ul class="nav">
                        <li><a href="newStudent.jsp">Add new student</a></li>
                        <li><a href="newTutor.jsp">Add new tutor</a></li>
                        <li><a href="tutorDetails.jsp">View Tutor Details</a></li>
                        <li><a href="clearAll.jsp" class='alert-danger'>Clear Everything</a></li>
                    </ul>
                </div>
            </div>
            <div class="hero-unit"> 
                <div class="row-fluid">
                    <div class="span4 text-center">
                        <h4>Students in line</h4>
                        <div id="Queue" class="span12 text-center"></div>
                    </div>
                    <div class="span8 text-center">
                        <h4>Tutors and current students</h4>
                        <div id="Tutors" class="span12 text-center"></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>