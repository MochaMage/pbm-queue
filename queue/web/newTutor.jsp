<%@page import="java.util.ArrayList"%>
<%@page import="Queue.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%QueueManager qm = QueueManager.singleton();%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add a new tutor</title>
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />
    </head>
    <body>
        <div class="container-fluid" style="padding-left: 10%; padding-right: 10%; width: 80%">
            <div class=" row-fluid"style="background-color: #336699">
                <br><h1 class="text-center" style="color: #FFFFFF">Queue Monitor</h1><br>
            </div>
            <div class="navbar">
                <div class="navbar-inner">
                    <a href="index.jsp" class="brand">Monitor</a>
                    <ul class="nav">
                        <li><a href="newStudent.jsp">Add new student</a></li>
                        <li><a href="newTutor.jsp">Add new tutor</a></li>
                        <li><a href="tutorDetails.jsp">View Tutor Details</a></li>
                        <li><a href="clearAll.jsp" class='alert-danger'>Clear Everything</a></li>
                    </ul>
                </div>
            </div>
            <div class="hero-unit">
                <h3>Add a new tutor</h3>
                 <form action="addTutor.jsp" method="POST" class="navbar-form">
                     <input type="text" placeholder="Name" name="name"><br/><br/>
                     <p>Choose what classes you can tutor</p>
                     <table class="table table-striped table-bordered">
                     <%
                         for(String s: qm.getSubjectList())
                         {
                             out.print("<tr><td>");
                             out.print("<input type='checkbox' class='pull-right' name='subjects' value='" + s + "'>");
                             out.print("<small>" + s +"</small>");
                             out.print("</tr></td>");
                         }
                     %>
                     </table>
                     <br/>
                     <button type="submit" style="margin-top: -5%" class="btn btn-primary text-center">Add Tutor</button>
                 </form>
            </div>
        </div>
    </body>
</html>