<%@page import="java.util.ArrayList"%>
<%@page import="Queue.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%QueueManager qm = QueueManager.singleton();%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add a new tutor</title>
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />
    </head>
    <body>
        <div class="container-fluid" style="padding-left: 10%; padding-right: 10%; width: 80%">
            <div class=" row-fluid"style="background-color: #336699">
                <br><h1 class="text-center" style="color: #FFFFFF">Queue Monitor</h1><br>
            </div>
            <div class="navbar">
                <div class="navbar-inner">
                    <a href="index.jsp" class="brand">Monitor</a>
                    <ul class="nav">
                        <li><a href="newStudent.jsp">Add new student</a></li>
                        <li><a href="newTutor.jsp">Add new tutor</a></li>                        
                        <li><a href="tutorDetails.jsp">View Tutor Details</a></li>
                        <li><a href="clearAll.jsp" class='alert-danger'>Clear Everything</a></li>
                    </ul>
                </div>
            </div>
            <div class="hero-unit">
                <%
                    String name = request.getParameter("name");
                    String subject = request.getParameter("subject");
                    
                    if(name.equals("") || subject == null)
                    {
                        out.print("<h3 class='text-error text-center'>");
                        out.print("Invalid input. Please input your name and a subject.");
                        out.print("<br/>");
                        out.print("<a href='newStudent.jsp' class='btn btn-primary'>Go back.</a>");
                        out.print("</h3>");
                    }
                    else
                    {
                        Request r = new Request(name, subject);
                        qm.addStudent(r);
                        out.print("<h3 class='text-success'>");
                        out.print("Student <em>" + name + "</em> added to Queue.");
                        out.print("</h3>");
                        out.print("<p class='text-info'>Chosen Subject: " + subject + "</p>");
                        out.print("<a href='index.jsp' class='btn btn-primary'>Go to monitor.</a>");
                    }
                %>
            </div>
        </div>
    </body>
</html>