<%@page import="java.util.ArrayList"%>
<%@page import="Queue.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%QueueManager qm = QueueManager.singleton();%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add a new student</title>
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />
    </head>
    <body>
        <div class="container-fluid" style="padding-left: 10%; padding-right: 10%; width: 80%">
            <div class=" row-fluid"style="background-color: #336699">
                <br><h1 class="text-center" style="color: #FFFFFF">Queue Monitor</h1><br>
            </div>
            <div class="navbar">
                <div class="navbar-inner">
                    <a href="index.jsp" class="brand">Monitor</a>
                    <ul class="nav">
                        <li><a href="newStudent.jsp">Add new student</a></li>
                        <li><a href="newTutor.jsp">Add new tutor</a></li>                        
                        <li><a href="tutorDetails.jsp">View Tutor Details</a></li>
                        <li><a href="clearAll.jsp" class='alert-danger'>Clear Everything</a></li>
                    </ul>
                </div>
            </div>
            <div class="hero-unit">
                <div class="row-fluid">
                <h3>Add a new student</h3>
                <%
                    if(qm.availableSubjects().isEmpty())
                            out.print("<h3 class='text-info text-center'>No tutors currently available</h3>");
                    else
                    {
                        out.print("<form action='addStudent.jsp' method='POST' class='navbar-form'>");
                        out.print("<input type='text' placeholder='Name' name='name'><br/><br/>");
                        out.print("<p>Subjects Available</p>");
                        out.print("<table class='table table-striped table-bordered'>");
                        for(String s: qm.availableSubjects())
                        {
                            out.print("<tr><td>");
                            out.print("<input type='radio' class='pull-right' name='subject' value='"+ s +"'>");
                            out.print("<small>");
                            out.print(s);
                            out.print("</small>");
                            out.print("</td></tr>");
                        }
                        out.print("</table>");
                        out.print("<button type='submit' class='btn btn-primary'>Add student</button>");
                        out.print("</form>");
                    }
                %>
                </div>
            </div>
        </div>
    </body>
</html>