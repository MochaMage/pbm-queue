package Queue;

import java.util.ArrayList;


/**
 *
 * @author Mocha
 */
public class Tutor implements Comparable
{
    private Request currentStudent;
    private String name;
    private int totalRequests;
    private ArrayList<String> subjects;
    
    /**
     * Creates a new instance of a Tutor object.
     * @param n The name of the tutor being added.
     * @param c The list of subjects that this tutor can help with.
     */
    public Tutor(String n, ArrayList<String> c)
    {
        name = n;
        totalRequests = 0;
        subjects = c;
        currentStudent = null;
    }
    
    /**
     * Changes the current student to whomever will see this Tutor next.
     * @param r Contains the next Request that will be served by the Tutor.
     */
    public void serveNextStudent(Request r)
    {
        if(r == null)
            currentStudent = r;
        else
        {
            currentStudent = r;
            totalRequests++;
        }
    }
    
    /**
     * Returns the current student being served by this Tutor.
     * @return the current Request being served by this Tutor. Returns null 
     * when Tutor is free.
     */
    public Request getCurrent()
    {
        return currentStudent;
    }
    
    /**
     * Returns the name of the tutor.
     * @return The name of the Tutor
     */
    public String getName()
    {
        return name;
    }   
    
    /**
     * Returns the total number of Requests served by this tutor in total.
     * @return The total number of Requests served by this tutor in total.
     */
    public int requestsServed()
    {
        return totalRequests;
    }
    
    /**
     * Checks whether a tutor can help out with the given course.
     * @param course The course we are checking if this tutor can help with.
     * @return Returns whether this tutor can help with the given course.
     */
    public boolean canTutor(String course)
    {
        return subjects.contains(course);
    }
    
    public ArrayList<String> getSubjects()
    {
        return subjects;
    }

    @Override
    public int compareTo(Object o) 
    {
        return  this.requestsServed() - ((Tutor)o).requestsServed();
    }

    @Override
    public boolean equals(Object o)
    {
        Tutor other = (Tutor) o;
        return this.getName().equalsIgnoreCase(other.getName());
    }
}
