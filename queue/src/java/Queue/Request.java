package Queue;

/**
 *
 * @author Mocha
 */
public class Request 
{
    private String studentName;
    private String subject;
    
    /**
     * Creates a new tutoring request.
     * @param sid The name of the student who's made this Request
     * @param c The subject that the student needs help with.
     */
    public Request(String s, String c)
    {
        studentName = s;
        subject = c;
    }
    
    /**
     *
     * @return The subject being requested for help with.
     */
    public String getSubject()
    {
        return subject;
    }
    
    /**
     * 
     * @return The name of the student who made the request.
     */
    public String getName()
    {
        return studentName;
    }
}