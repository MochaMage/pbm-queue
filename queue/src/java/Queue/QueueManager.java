package Queue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author Mocha
 */
public class QueueManager 
{
    private Queue<Request> mq;
    private ArrayList<Tutor> tutors;
    private static QueueManager current;
    private final String[] SUBJECTS = {"Calculus",
                                        "Algebra", 
                                        "Java", 
                                        "Discrete Math",
                                        "Numerical Analysis", 
                                        "C++", 
                                        "Theory of Algorithms", 
                                        "Logic for CS", 
                                        "Data Structures", 
                                        "Principles of Programming Languages"};
    
    private QueueManager()
    {
        mq = new LinkedList();
        tutors = new ArrayList<Tutor>();
    }
    
    /**
     * Returns the current static instance of the queue manager class.
     * @return Returns the Queue Manager singleton. This singleton is
     * the manager of queuing students. 
     */
    public static QueueManager singleton()
    {
        if(current == null)
            current = new QueueManager();
        return current;
    }
    
    /**
     * Adds a new tutor to the arraylist of tutors.
     * @param t The Tutor that is to be added to the tutor arraylist.
     * @return  True if no other tutor with the same name was found, and false 
     *  if there were duplicates found.
     */
    public boolean addTutor(Tutor t)
    {
        for(Tutor e: tutors)
            if(e.equals(t))
            {
                return false;
            }
        tutors.add(t);
        return true;
    }
    
    /**
     * Adds a Request to the main queue.
     * @param r The Request object that is to be added to the queue. 
     */
    public void addStudent(Request r)
    {
        mq.offer(r);
    }
    
    /**
     * Shows the students currently with the tutors.
     * @return Arraylist containing the students being served by a Tutor
     * at the moment. Returns all tutors currently in the arraylist.
     */
    public ArrayList<Tutor> currentTutors()
    {
        return tutors;
    }
    
    public void clearAll()
    {
        tutors.clear();
        mq.clear();
    }
    
    /**
     * Shows the list of subjects that can currently be requested for help with.
     * @return an ArrayList<String> of subjects.
     */
    public ArrayList<String> availableSubjects()
    {
        ArrayList<String> subjects = new ArrayList<String>();
        for(Tutor t:tutors)
        {
            for(String s: t.getSubjects())
            {
                if(!subjects.contains(s))
                {
                    subjects.add(s);
                }
            }
        }    
        Collections.sort(subjects);
        return subjects;
    }
    
    /**
     * Gives the list of subjects that a tutor may choose to tutor.
     * @return An array of subject names Strings.
     */
    public String[] getSubjectList()
    {
        return SUBJECTS;
    }
    
    /**
     * Returns the students currently in the queue that have not been served
     * by a tutor yet.
     * @return The arraylist of all Requests in the main queue.
     */
    public ArrayList<Request> queueState()
    {
        Iterator<Request> itr = mq.iterator();
        ArrayList<Request> ret = new ArrayList<Request>();
        
        while(itr.hasNext())
        {
            ret.add(itr.next());
        }        
        return ret;
    }
        
    /**
     * Sits the next batch of students down with the current list of tutors.
     * Uses prioritized bipartite matching (PBM) in order to find the maximum matching
     * of students in the queue with respect to their queue priority. After 
     * finding a maximum match, load balancing matching is then performed in 
     * order to balance the amount of students that a Tutor serves in total. Any
     * students who were skipped over by the PBM are added to the front of the 
     * queue so that they may be served next time PBM is run.
     */
    public void sitNextStudents()
    {
        ArrayList<Request> matches = new ArrayList<Request>();
        ArrayList<boolean[]> bGraphs = new ArrayList<boolean[]>();
        Queue<Request> rejects = new LinkedList();
        BipartiteMatcher m = new BipartiteMatcher();
        int matchSize = 0;        
        int[] tutIdx;
        
        if(mq.size() == 0)
        {
            for(Tutor t: tutors)
                t.serveNextStudent(null);
            return;
        }
                
        /*
         * The sort is done purely for the purpose of load balancing on the tutors.
         * The sort returns the tutors list again in descending order based on the 
         * number of requests a tutor has helped previously.
         */
        Collections.sort(tutors);        
        /*
         * Serialized Maximum Matching. At the end of the loop, the BipartiteMatcher
         * will contain the indexes of the matched students and tutors.
         */
        while(matchSize < tutors.size())
        {
            Request nextInLine = mq.poll();
            
            /*
             * The check for null is done so that we are covered in case that the queue
             * is short and runs out before we can fill up each and every tutor.
             */
            if(nextInLine == null)
                break;
            //Each position in nextBool corresponds to the tutor's position in the tutors' array.
            //Position 0 in nextBool indicates whether tutors[0] can help out nextInLine request.
            boolean[] nextBool = new boolean[tutors.size()];
            for(int i = 0; i < tutors.size(); i++)
            {
                nextBool[i] = tutors.get(i).canTutor(nextInLine.getSubject());
            }
            bGraphs.add(nextBool);             
            
            /*
             * The following two lines are in order to create the boolean[][] array
             * that the BipartiteMatcher class requires. 
             */
            boolean[][] graph = new boolean[bGraphs.size()][bGraphs.get(0).length];            
            bGraphs.toArray(graph);
                        
            /*
             * This check is done in order to ensure that FCFS priority of the queue is 
             * respected. If not every student was matched to a tutor, that means that 
             * there exists a student that was not matched, and that it might be someone
             * who signed into the queue earlier. The student is taken off and put into
             * a rejects queue, which will become the head of the main queue once the 
             * matching process is completed. However, if there is a perfect matching,
             * meaning every student was matched, then keep the student on the current 
             * matching adjacency list.
             */
            if((matchSize = m.maximumMatching(graph)) != bGraphs.size())
            {
                bGraphs.remove(bGraphs.size() - 1);
                rejects.add(nextInLine);
            }
            else
            {
                matches.add(nextInLine);
            }
        }          
        
        /*
         * By the time this line is reached, the last run of maximumMatching()
         * should have the best matching for the first students in line. Now, we
         * save them before applying load balancing.
         */
        tutIdx = m.rightMatches();  
        
        /*
         * Load balancing. Check whether all tutors are taken. If some are idle, 
         * The busiest tutor will be removed. Matching will be done to see if that tutor 
         * Was needed at all and if it can be afforded to idle him for this volley of matchings. 
         * The tutor's list earlier was sorted such that the first element would be the tutor with 
         * the largest workload so far. Each iteration of the loop in the following if-statement
         * Should remove the busiest tutor. If the returned match size is the same as the previous
         * then the loop is allowed to keep running. This should put the tutors who have been the 
         * most idle to work
         */        
        if(matchSize < tutors.size() && matchSize > 0)
        {            
            /*
             * The following loop removes the rightmost tutor from each
             * requests' adjacency matrices. This check will keep occuring
             * until we reach the most minimal match in which the busiest tutor 
             * needs to be kept.
             */
            for(int i = 1;  bGraphs.get(0).length - i != 0 ; i++)
            {
                boolean[][] graph = new boolean[bGraphs.size()][bGraphs.get(0).length - i];
                
                for(int j = 0; j < bGraphs.size(); j++)
                {
                    System.arraycopy(bGraphs.get(j), 0, graph[j], 0, bGraphs.get(0).length - i);
                }
                
                if(m.maximumMatching(graph) == matchSize)
                    tutIdx = m.rightMatches();
                else
                    break;                
            }
        }
        
        /*
         * Do the actual tutor assigning here.
         */
        if(tutIdx != null && tutIdx.length != 0)
        {
            for(int i = 0; i < tutIdx.length; i++)
            {
                if(tutIdx[i] == -1)
                    tutors.get(i).serveNextStudent(null);
                else
                    tutors.get(i).serveNextStudent(matches.get(tutIdx[i]));
            }    
        }
        //Merge the rejects from this volley of matchings with the main queue and replace.
        //This guarantees that at least 1 reject from the last volley will be served next.
        Request r;
        while((r = mq.poll()) != null)
        {rejects.add(r);}
        mq = rejects;
    }
    
    private class BipartiteMatcher
    {
        /*
         * Original Bipartite Matching Code created by GitHub User Irene Papakonstantinou. 
         * Added a constructor, as well as accessor methods for match arrays.
         */
        private int m, n;
        private boolean[][] graph;
        private boolean seen[];
        private int matchL[];   //What left vertex i is matched to (or -1 if unmatched)
        private int matchR[];   //What right vertex j is matched to (or -1 if unmatched)
 
        private int[] leftMatches()
        {
            return matchL;
        }
 
        private int[] rightMatches()
        {
            return matchR;
        }

        private int maximumMatching(boolean [][] g)
        {
            graph = g;
            m = graph.length;
            n = graph[0].length;
            matchL = new int[m];
            matchR = new int[n];
            seen = new boolean[n];
            
            Arrays.fill(matchL, -1);
            Arrays.fill(matchR, -1);
            int count = 0;
            for (int i = 0; i < m; i++) 
            {
                Arrays.fill(seen, false);
                if (bpm(i)) count++;
            }
            return count;
        }

        private boolean bpm(int u) 
        {
            for (int v = 0; v < n; v++) 
            {
                if (!graph[u][v] || seen[v]) continue;
                seen[v] = true;
                //match u and v, if v is unassigned, or if v's match on the left side can be reassigned to another right vertex
                if (matchR[v] == -1 || bpm(matchR[v])) 
                {
                    matchL[u] = v;
                    matchR[v] = u;
                    return true;
                }
            }
            return false;
        }  
    }
}