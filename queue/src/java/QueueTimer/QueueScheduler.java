package QueueTimer;

import Queue.QueueManager;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class QueueScheduler implements ServletContextListener
{
    private final long PERIOD = 90000;
            
    @Override
    public void contextInitialized(ServletContextEvent sce) 
    {
        ServletContext sc = sce.getServletContext();
        try
        {
            Timer timer = new Timer();
            TimerTask task = new TimerTask() 
            {
                @Override
                public void run() 
                {
                    QueueManager qm = QueueManager.singleton();
                    qm.sitNextStudents();
                    System.out.println("Queue Scheduler: Cycle complete, next cycle started.");
                }
            };
            Calendar calendar = Calendar.getInstance();
            Date startTime = calendar.getTime();
            timer.scheduleAtFixedRate(task, startTime, PERIOD);
            sc.setAttribute("Timer", timer);
            System.out.println("Queue Timer started");
        }
        catch(Exception e)
        {
            sc.log("Timer not initialized: " + e.getMessage());
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) 
    {
        ServletContext sc = sce.getServletContext();
        Timer timer = (Timer) sc.getAttribute("Timer");
        if(timer != null)
            timer.cancel();
        sc.removeAttribute("Timer");
        System.out.println("Queue Timer Terminated");
    }
    
}
