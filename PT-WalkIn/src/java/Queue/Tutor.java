package Queue;

import java.util.ArrayList;


public class Tutor implements Comparable
{
    private Request currentStudent;
    private String pid;
    private int totalRequests;
    private ArrayList<String> courses;
    
    public Tutor(String p, ArrayList<String> c)
    {
        pid = p;
        totalRequests = 0;
        courses = c;
    }
    
    public void serveNextStudent(Request r)
    {
        currentStudent = r;
    }
    
    public Request getCurrent()
    {
        return currentStudent;
    }
    
    public String getPid()
    {
        return pid;
    }   
    
    public int requestsServed()
    {
        return totalRequests;
    }
    
    //Purely for testing, remove once done
    public void setRN(int n)
    {
        this.totalRequests = n;
    }
    
    public boolean canTutor(String course)
    {
        return courses.contains(course);
    }

    @Override
    public int compareTo(Object o) 
    {
        return ((Tutor)o).requestsServed() - this.requestsServed();
    }

    @Override
    public boolean equals(Object o)
    {
        String otherPid = (String) o;
        return this.getPid().equalsIgnoreCase(otherPid);
    }
}
