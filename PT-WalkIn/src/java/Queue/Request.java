package Queue;

public class Request 
{
    private String studentID;
    private String course;
    
    public Request(String sid, String c)
    {
        studentID = sid;
        course = c;
    }
    
    public String getCourse()
    {
        return course;
    }
    
    public String getPid()
    {
        return studentID;
    }
}