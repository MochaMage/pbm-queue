package Queue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

public class QueueManager 
{
    private Queue<Request> mq;
    private ArrayList<Tutor> tutors;
    private static QueueManager current;
    
    private QueueManager()
    {
        mq = new LinkedList();
        tutors = new ArrayList<Tutor>();
    }
    
    public static QueueManager singleton()
    {
        if(current == null)
            current = new QueueManager();
        return current;
    }
    
    public void addTutor(Tutor t)
    {
        tutors.add(t);
    }
    
    public void enqueue(Request r)
    {
        mq.offer(r);
    }
        
    public void sitNextStudents()
    {
        ArrayList<Request> matches = new ArrayList<Request>();
        ArrayList<boolean[]> bGraphs = new ArrayList<boolean[]>();
        Queue<Request> rejects = new LinkedList();
        BipartiteMatcher m = new BipartiteMatcher(null);
        int matchSize = 0;
        
        /*
         * The sort is done purely for the purpose of load balancing on the tutors.
         * The sort returns the tutors list again in descending order based on the 
         * number of requests a tutor has helped previously.
         */
        Collections.sort(tutors);
        
        /*
         * Serialized Maximum Matching. 
         */
        while(matchSize < tutors.size())
        {
            Request nextInLine = mq.poll();
            
            /*
             * The check for null is done so that we are covered in case that the queue
             * is short and runs out before we can fill up each and every tutor.
             */
            if(nextInLine == null)
                break;
            //Each position in nextBool corresponds to the tutor's position in the tutors' array.
            //Position 0 in nextBool indicates whether tutors[0] can help out nextInLine request.
            boolean[] nextBool = new boolean[tutors.size()];
            for(int i = 0; i <= tutors.size(); i++)
            {
                nextBool[i] = tutors.get(i).canTutor(nextInLine.getCourse());
            }
            bGraphs.add(nextBool); 
            
            boolean[][] graph = new boolean[bGraphs.size()][bGraphs.get(0).length];
            
            for(int i = 0; i < bGraphs.size(); i++)
            {
                System.arraycopy(bGraphs.get(i), 0, graph[i], 0, bGraphs.get(0).length);
            }
            
            m = new BipartiteMatcher(graph);
            
            /*
             * This check is done in order to ensure that FCFS priority of the queue is 
             * respected. If not every student was matched to a tutor, that means that 
             * there exists a student that was not matched, and that it might be someone
             * who signed into the queue earlier. The student is taken off and put into
             * a rejects queue, which will become the head of the main queue once the 
             * matching process is completed. However, if there is a perfect matching,
             * meaning every student was matched, then keep the student on the current 
             * matching adjacency list.
             */
            if((matchSize = m.maximumMatching()) == bGraphs.size())
            {
                matches.add(nextInLine);
            }
            else
            {
                bGraphs.remove(bGraphs.size() - 1);
                rejects.add(nextInLine);
            }
        }        
        /*
         * Load balancing. Check whether all tutors are taken. If some are idle, 
         * The busiest tutor will be removed. Matching will be done to see if that tutor 
         * Was needed at all and if it can be afforded to idle him for this volley of matchings. 
         * The tutor's list earlier was sorted such that the first element would be the tutor with 
         * the largest workload so far. Each iteration of the loop in the following if-statement
         * Should remove the busiest tutor. If the returned match size is the same as the previous
         * then the loop is allowed to keep running. This should put the tutors who have been the 
         * most idle to work
         */        
        if(matchSize < tutors.size())
        {            
            /*
             * Instead of remaking the boolean[][], only remove the first element of each
             * array in the bGraphs arraylist
             */
            
            //"Remove" the tutor with the highest request number, see if the same matchsize is reached
        }
        
        int[] tutIdx = m.matchR;
        
        //Merge the rejects from this volley of matchings with the main queue and replace.
        //This guarantees that at least 1 reject from the last volley will be served next.
        Request r;
        while((r = mq.poll()) != null)
        {rejects.add(r);}
        mq = rejects;
        
        /*
         * Remember to actually assign requests to tutors.
         */
    }
    
    private class BipartiteMatcher
    {
        /*
         * Original Bipartite Matching Code created by GitHub User Irene Papakonstantinou. 
         * Added a constructor, as well as accessor methods for match arrays.
         */
        private int m, n;
        private boolean[][] graph;
        private boolean seen[];
        private int matchL[];   //What left vertex i is matched to (or -1 if unmatched)
        private int matchR[];   //What right vertex j is matched to (or -1 if unmatched)
    
        public BipartiteMatcher(boolean [][] g)
        {
            graph = g;
            m = graph.length;
            n = graph[0].length;
            matchL = new int[m];
            matchR = new int[n];
            seen = new boolean[n];        
        }
 
        public int[] leftMatches()
        {
            return matchL;
        }
 
        public int[] rightMatches()
        {
            return matchR;
        }

        public int maximumMatching()
        {
            Arrays.fill(matchL, -1);
            Arrays.fill(matchR, -1);
            int count = 0;
            for (int i = 0; i < m; i++) 
            {
                Arrays.fill(seen, false);
                if (bpm(i)) count++;
            }
            return count;
        }

        private boolean bpm(int u) 
        {
            for (int v = 0; v < n; v++) 
            {
                if (!graph[u][v] || seen[v]) continue;
                seen[v] = true;
                //match u and v, if v is unassigned, or if v's match on the left side can be reassigned to another right vertex
                if (matchR[v] == -1 || bpm(matchR[v])) 
                {
                    matchL[u] = v;
                    matchR[v] = u;
                    return true;
                }
            }
            return false;
 }  
 }
}