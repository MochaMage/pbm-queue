<%-- 
    Document   : QueueMonitor
    Created on : Jul 18, 2013, 10:02:24 AM
    Author     : Mocha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Queue Monitor</title>
    </head>
    <body>
        <h1 id="timer"></h1>
        <script>
            var myVar=setInterval(function(){myTimer()},1000);
            function myTimer()
            {
                var d=new Date();
                var t=d.toLocaleTimeString();
                document.getElementById("timer").innerHTML=t;
            }
        </script>
    </body>
</html>
